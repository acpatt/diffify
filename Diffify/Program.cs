﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO.Compression;


namespace Diffify
{
    class Program
    {

        /// <summary>
        /// Checks if it a file exists at the path and whether it can open it as an archive
        /// </summary>
        /// <param name="path"></param>
        /// <returns>
        /// True if it exists and can be opened else False
        /// </returns>
        protected static bool IsArchive(string path)
     
   {
            bool answer = false;
            if (File.Exists(path))
            {
                try
                {
                    var archive = ZipFile.OpenRead(path);
                    return archive.Entries.Count > 0;
                }
                catch (Exception)
                {
                    answer = false;
                    return answer;
                }
            }
            return answer;
        }

        /// <summary>
        /// Parses the arguments into a FileInfo object.
        /// </summary>
        /// <param name=”arguments”>a String Array of arguments </param>
        /// <returns>
        ///  a FileInfo object for the file in the argument
        /// </returns>
        /// <remarks>
        ///  exits if wrong number of arguments or file doesn’t exist
        /// </remarks>
        protected static FileInfo GetFileFromArgs(string[] arguments)
        {
            if (arguments.Length == 1)
            {
                string path = arguments[0];
                if (IsArchive(path))
                {
                    return new FileInfo(path);
                }
                else
                {
                    Console.WriteLine("Data Exception, non-existent or invalid file");
                    Environment.Exit(1);
                    return null;
                }
            }
            else
            {
                Console.WriteLine("Argument Exception, there wasn't one argument");
                Environment.Exit(1);
                return null;
            }
        }

        /// <summary>
        /// Takes a FileInfo object and unzips it into a directory
        /// </summary>
        /// <param name="sourceArchive">The source file to unzip.</param>
        /// <returns>
        ///  A DirectoryInfo object into which the source archive has been unzipped.
        /// </returns>
        /// <remarks>
        ///  Exits if the source file isn’t an archive
        /// </remarks>
        protected static DirectoryInfo UnzipArchive(FileInfo archive)
        {
            FileStream archiveStream = new FileStream(archive.FullName, FileMode.Open);
            ZipArchive sourceArchive = new ZipArchive(archiveStream, ZipArchiveMode.Read);
            string directoryPath = string.Concat(Path.GetDirectoryName(archive.FullName), '\\',  Path.GetFileNameWithoutExtension(archive.FullName));
            if (Directory.Exists(directoryPath)) {
                Console.WriteLine("The directory's already there!");
                Directory.Delete(directoryPath);
                Console.WriteLine("Replacing the directory.");
            }
            sourceArchive.ExtractToDirectory(directoryPath);
            return new DirectoryInfo(directoryPath);
        }

        /// <summary>
        /// Takes a DirectoryInfo object and enumerates the contents.
        /// </summary>
        /// <param name="unzipDirectory">the directory to which the archive has been unzipped</param>
        /// <returns>
        ///  an IEnumerable<System.IO.FileInfo> containing all the files in the directory
        /// </returns>
        /// <remarks>
        /// probably not the most necessary
        /// </remarks>
        protected static IEnumerable<FileInfo> EnumerateFiles(DirectoryInfo unzipDirectory) => unzipDirectory.EnumerateFiles("*", SearchOption.AllDirectories);

        /// <summary>
        /// Takes a directory you give it and recursively expands every archive it finds inside.
        /// </summary>
        /// <param name="extractedDirectory"></param>
        protected static void ParseDirectoryForArchives(DirectoryInfo extractedDirectory)
        {
            IEnumerable<System.IO.FileInfo> directoryContents = EnumerateFiles(extractedDirectory);
            foreach (FileInfo file in directoryContents)
            {
                if (IsArchive(file.FullName))
                {
                    ParseDirectoryForArchives(UnzipArchive(file));
                }
            }
            return;
        }

        /// <summary>
        /// checks a FileInfo to see if it’s an xml (or Rels) under 100MB
        /// </summary>
        /// <param name="file">the prospective xml</param>
        /// <returns>
        ///  True if the file is xml and under 100MB otherwise False
        /// </returns>
        /// <remarks>
        ///  Should check the contents of file, not just name
        /// </remarks>
        protected static bool IsValidXml(FileInfo file)
        {
            string[] XMLExtensions = { ".xml", ".rels" };
            bool hasValidExtension = XMLExtensions.Any(ext => file.Extension.IndexOf(ext, StringComparison.OrdinalIgnoreCase) >= 0);
            bool isUnder100MB = (file.Length <= 104857600);
            if (hasValidExtension && isUnder100MB)
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// reads in a file, parses it and then writes it back
        /// </summary>
        /// <param name="file">fileinfo xml file to parse </param>
        /// <returns>
        ///  void
        /// </returns>
        /// <remarks>
        ///  needs chunking
        /// </remarks>
        protected static void ParseXml(FileInfo file)
        {
            string backupPath = String.Concat(file.FullName, ".backup");
            if (File.Exists(backupPath))
            {
                Console.WriteLine("The backup file's already there!");
                Directory.Delete(backupPath);
                Console.WriteLine("Replacing the backup file.");
            }
            File.Copy(file.FullName, backupPath);
            FileInfo fileCopy = new FileInfo(backupPath);
            File.Delete(file.FullName);
            XmlDocument sourceXml = new XmlDocument();
            sourceXml.Load(fileCopy.FullName);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.Indent = true;
            settings.NewLineOnAttributes = true;
            sourceXml.Save(XmlWriter.Create(file.FullName, settings));
            File.Delete(fileCopy.FullName);
        }

        static void Main(string[] args)
        {
            FileInfo sourceArchive = GetFileFromArgs(args);
            DirectoryInfo extractDirectory = UnzipArchive(sourceArchive);
            ParseDirectoryForArchives(extractDirectory);
            IEnumerable<System.IO.FileInfo> files = EnumerateFiles(extractDirectory);
            foreach (FileInfo file in files) {
                if (IsValidXml(file))
                {
                    ParseXml(file);
                }
            }
        }

    }
}
